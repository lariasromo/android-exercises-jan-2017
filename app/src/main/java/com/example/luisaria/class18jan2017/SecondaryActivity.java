package com.example.luisaria.class18jan2017;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Toast;
import android.graphics.Color;
import android.support.design.widget.*;

public class SecondaryActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_secondary);
    Button button = (Button) findViewById(R.id.button5);
    registerForContextMenu(button);
    getSupportActionBar().setHomeButtonEnabled(true);
    getSupportActionBar().setDisplayShowHomeEnabled(true);
    getSupportActionBar().setDisplayUseLogoEnabled(true);
    getSupportActionBar().setLogo(R.mipmap.ic_launcher);
  }

  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    super.onCreateContextMenu(menu, v, menuInfo);
    getMenuInflater().inflate(R.menu.options_menu , menu);
  }

  public void toast(View v){
    Toast.makeText(this, "Hello there", Toast.LENGTH_LONG).show();
  }

  public void snackbar(View v){
    Snackbar.make(v, "Hello There", Snackbar.LENGTH_LONG).setAction("Action", null).show();

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.options_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle item selection
    switch (item.getItemId()) {
      case R.id.new_game:
        Toast.makeText(this, "New Game", Toast.LENGTH_LONG).show();
        return true;
      case R.id.help:
        Toast.makeText(this, "Ayuda", Toast.LENGTH_LONG).show();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }
  @Override
  public boolean onContextItemSelected(MenuItem item) {

    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

    switch (item.getItemId()) {
      case R.id.new_game:
        Toast.makeText(this, "New Game", Toast.LENGTH_LONG).show();
      case R.id.help:
        Toast.makeText(this, "Ayuda", Toast.LENGTH_LONG).show();
      default:
        Toast.makeText(this, "Otro", Toast.LENGTH_LONG).show();
    }
    return true;
  }

  public void alert(View v){
    showDialog("Hello There");
  }

  public void showDialog(String message){
    AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
    builder1.setMessage(message);
    builder1.setCancelable(true);
    builder1.setPositiveButton(
      "SI",
      new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {dialog.cancel();
        }
      });
    builder1.setNegativeButton(
      "NO",
      new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
          dialog.cancel();
        }
      });
    AlertDialog alert11 = builder1.create();
    alert11.show();

  }

  public void popUpMenu(View v) {
    Button button = (Button) findViewById(R.id.button5);
    //Creating the instance of PopupMenu
    PopupMenu popup = new PopupMenu(SecondaryActivity.this, button);
    //Inflating the Popup using xml file
    popup.getMenuInflater()
      .inflate(R.menu.options_menu, popup.getMenu());

    //registering popup with OnMenuItemClickListener
    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
      public boolean onMenuItemClick(MenuItem item) {
        Toast.makeText(
          SecondaryActivity.this,
          "You Clicked : " + item.getTitle(),
          Toast.LENGTH_SHORT
        ).show();
        return true;
      }
    });

    popup.show(); //showing popup menu
  }

}
